package main

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"strings"
	"time"

	"github.com/bsm/nanoid"
	"gitlab.com/bsm/challenges/memdb.git/internal/memdb"
)

var countries = []string{
	"AT",
	"BE",
	"CA",
	"CH",
	"DE", "DE",
	"DK",
	"ES",
	"FI",
	"FR", "FR",
	"GB", "GB",
	"IT", "IT",
	"NL",
	"NO",
	"PT",
	"SE",
	"US", "US", "US", "US", "US",
}

var colours = []string{
	"aqua", "black", "blue", "fuchsia", "gray", "green",
	"lime", "maroon", "navy", "olive", "orange", "purple",
	"red", "silver", "teal", "white", "yellow",
}

var queries = []*memdb.Query{
	{
		Country:    "GB",
		Background: "navy",
		Account:    65,
	},
	{
		Country: "NL",
		Account: 34,
	},
	{
		Country:   "US",
		Placement: 6,
	},
}

func main() {
	if err := run(); err != nil {
		log.Fatalln(err)
	}
}

func run() error {
	ech := make(chan *memdb.Event, 100)
	go func() {
		rnd := rand.New(rand.NewSource(2021))
		for i := 0; i < 1e8; i++ {
			ech <- newEvent(rnd)
		}
		close(ech)
	}()

	store := memdb.New()
	count := 0
	start := time.Now()
	for evt := range ech {
		store.Put(evt)
		if count++; count%1e7 == 0 {
			fmt.Printf("ingested %d events in %.1fs\n", count, time.Since(start).Seconds())
		}
	}

	for _, query := range queries {
		fmt.Printf("%+v\n", store.Query(query))
	}
	return nil
}

func newEvent(r *rand.Rand) *memdb.Event {
	rs := nanoid.Must(nanoid.Base32.FromEntropy(r, 3))
	n1 := r.Intn(len(countries))
	n2 := r.Intn(len(colours))
	return &memdb.Event{
		DeviceID:   strings.Repeat(rs, 8),
		Country:    countries[n1],
		Background: colours[n2],
		Placement:  r.Int63n(10) + 1,
		Account:    r.Int63n(100) + 1,
		Price:      seedPrice(r, float64(n1)/1.9, float64(n2)/1.4),
	}
}

func seedPrice(r *rand.Rand, mean, stdev float64) float64 {
	p := mean + r.NormFloat64()*stdev
	return math.Floor(100*p) / 100
}
