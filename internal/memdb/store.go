package memdb

// A Query acts as a filter to select the relevant records in the Store. For
// example, &Query{Country: "GB", Account: 123} will return a Summary using all
// Records where Country equals "GB" and Account equals 123.
type Query struct {
	Country    string
	Background string
	Placement  int64
	Account    int64
}

// A Summary is returned for user queries by the Store.
type Summary struct {
	NumEvents   int     // the number of events seen
	PriceMean   float64 // the average price
	PriceStdDev float64 // the standard deviation of price
	Frequency   int     // the (approximate) number of unique users (98% accurate)
}

// A Store is a thread-safe, in-memory data store which ingests Event records
// and can be queried for aggregate summaries.
type Store interface {
	Put(*Event)
	Query(*Query) *Summary
}

func New() Store {
	panic("not implemented")
}
