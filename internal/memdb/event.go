package memdb

// An Event represents a single incoming event.
type Event struct {
	DeviceID   string
	Country    string
	Background string
	Placement  int64
	Account    int64
	Price      float64
}
