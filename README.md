# Go: MemDB Challenge

For our real-time data analysis, we want to create a small and efficient event database which runs entirely in memory.

Please follow these steps:

1. Fork and clone this repository.
2. Implement the `internal/memdb.Store` interface.
3. Update the `internal/memdb.New()` function to return an instance of your `Store` implementation.
4. Finally, run the `go run cmd/memdb-challenge/main.go` and note the output.
5. Submit a pull/merge request for our review and paste the output if the script above in the description.

Additional notes/hints:

- Follow best code practices.
- Pay attention to comments in the code.
- Make sure your implementation is memory efficient.
- Query performance is not critical.
